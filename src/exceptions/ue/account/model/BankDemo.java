package exceptions.ue.account.model;

import exceptions.ue.account.exception.AccountNotFoundException;
import exceptions.ue.account.exception.BankException;

public class BankDemo {
	
	public static void main(String[] args) {
		
		Bank bank = new Bank();
		String owner = "";		
		String onwer1 = "Max Mustermann";
		String onwer2 = "Maria MusterFrau";
		String onwer3 = "Mario Jedermann";
		
		
		try {
			owner = onwer1;
			bank.addAccount(owner, 100.0, 10);
			System.out.println("Owner: [ " + owner + "] wurde erfolgreich zu Account-List hinzugefügt");
			
			owner = onwer2;
			bank.addAccount(owner, 50, 20);
			System.out.println("Owner: [" + owner + "] wurde erfolgreich zu Account-List hinzugefügt \n");
			
			bank.transfer(onwer1, onwer2, 100);
			System.out.println("Betrag wurde von:" + onwer1 + "zu: " + onwer2 + "  überwiesen");
			
			//NotEnoughException
//			bank.transfer(onwer1, onwer2, 100);
			
			//AccountNotFoundException
			owner = onwer1;
			bank.transfer(owner, onwer3, 100);			
					
		} catch (AccountNotFoundException e) {
			System.out.println("Owner: [ " + owner + "] account wurde nicht gefunden");
		} catch (BankException e) {
			System.out.println("Der Überziehungsrahmen wurde überzogen");
		}
	}

}
