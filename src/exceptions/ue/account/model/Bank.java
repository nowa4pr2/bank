package exceptions.ue.account.model;


import java.util.HashMap;
import java.util.Map;

import exceptions.ue.account.exception.AccountNotFoundException;
import exceptions.ue.account.exception.BankException;

public class Bank {

	private Map<String, Account> accounts = new HashMap<>();
	
	/**
	 * 
	 * @param owner -
	 * @param balance - 
	 * @param overdraftFram
	 * @throws BankException - wird geworfen falls der owner in der Account-List existiert
	 */
	public void addAccount(String owner, double balance, double overdraftFram) throws BankException {
		
		Account account = new Account(owner,balance,overdraftFram);
		
		if (accounts.containsKey(owner)) {
			throw new BankException();
		}
		
		accounts.put(owner, account);
		
	}
	/**
	 * 
	 * @param fromOwner - 
	 * @param toOwner
	 * @param amount
	 * @throws BankException
	 * @throws AccountNotFoundException 
	 */
	public void transfer(String fromOwner, String toOwner, double amount ) throws AccountNotFoundException, BankException {
		
		if (!accounts.containsKey(fromOwner) || !accounts.containsKey(toOwner)) {
			throw new AccountNotFoundException();
		}
		
		Account fromOWnerAccount = accounts.get(fromOwner);
		Account toOWnerAccount = accounts.get(toOwner);
		
		fromOWnerAccount.debit(amount);
		toOWnerAccount.credit(amount);		
		
	}

	public Map<String, Account> getAccounts() {
		return accounts;
	}	
	
}
